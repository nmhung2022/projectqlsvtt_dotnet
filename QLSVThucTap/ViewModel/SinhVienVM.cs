﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    public class SinhVienVM
    {
        public int STT { get; set; }
        public string MaSinhVien { get; set; }
        public string Ho { get; set; }
        public string Ten { get; set; }
        public string TenChuyenNganh { get; set; }
        public string TenDoanhNghiep { get; set; }
        public string TenGiangVienHuongDan { get; set; }
        public string GioiTinh { get; set; }
        public DateTime NamSinh { get; set; }

        //Other
        public string TenKhoa { get; set; }
        public string TenLopHocPhan { get; set; }
        public string MaChuyenNganh { get; set; }
        public string MaDoanhNghiep { get; set; }
        public string MaGiangVienHuongDan { get; set; }
        public string MaLopHocPhan { get; set; }
    }
}

