﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    public class LopHocPhanVM
    {
        public string MaLopHocPhan { get; set; }
        public string TenLopHocPhan { get; set; }
        public string MaKhoa { get; set; }
    }
}
