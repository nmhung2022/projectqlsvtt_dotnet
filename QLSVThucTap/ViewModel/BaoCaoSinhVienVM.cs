﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    internal class BaoCaoSinhVienVM
    {
        public int STT { get; set; }
        public string MaSinhVien { get; set; }
        public String Ho { get; set; }
        public String Ten { get; set; }
        public String MaGiangVien { get; set; }
        public String MaDoanhNghiep { get; set; }
        public String DeTai { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public String NoiDung { get; set; }
        //Other
        public String TenGiangVien { get; set; }
        public String TenDoanhNghiep { get; set; }
    }
}

