﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    internal class DoanhNghiepVM
    {
        public string MaDoanhNghiep { get; set; }
        public string TenDoanhNghiep { get; set; }
    }
}
