﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    public class GiangVienVM
    {
        public string MaGiangVien { get; set; }
        public string TenGiangVien { get; set; }
    }
}
