﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.ViewModel
{
    public class KhoaVM
    {
        public string MaKhoa { get; set; }
        public string TenKhoa { get; set; }
    }
}
