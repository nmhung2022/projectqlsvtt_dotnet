namespace QLSVThucTap.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BaoCaoCuaSinhVien")]
    public partial class BaoCaoCuaSinhVien
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int STT { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string MaSinhVien { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string MaDoanhNghiep { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string MaGiangVien { get; set; }

        public DateTime NgayBatDau { get; set; }

        public DateTime NgayKetThuc { get; set; }

        [Required]
        [StringLength(1000)]
        public string NoiDung { get; set; }

        [StringLength(350)]
        public string DeTai { get; set; }

        public virtual SinhVien SinhVien { get; set; }
    }
}
