namespace QLSVThucTap.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LopHocPhan")]
    public partial class LopHocPhan
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string MaLopHocPhan { get; set; }

        [Required]
        [StringLength(250)]
        public string TenLopHocPhan { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string MaGiangVienHuongDan { get; set; }

        [Required]
        [StringLength(50)]
        public string MaKhoa { get; set; }

        public virtual GiangVien GiangVien { get; set; }

        public virtual Khoa Khoa { get; set; }
    }
}
