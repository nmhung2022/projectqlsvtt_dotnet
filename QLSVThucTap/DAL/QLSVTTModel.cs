using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace QLSVThucTap.DAL
{
    public partial class QLSVTTModel : DbContext
    {
        public QLSVTTModel()
            : base("name=QLSVTTModelApp")
        {
        }

        public virtual DbSet<BaoCaoCuaSinhVien> BaoCaoCuaSinhViens { get; set; }
        public virtual DbSet<ChuyenNganh> ChuyenNganhs { get; set; }
        public virtual DbSet<DoanhNghiep> DoanhNghieps { get; set; }
        public virtual DbSet<GiangVien> GiangViens { get; set; }
        public virtual DbSet<Khoa> Khoas { get; set; }
        public virtual DbSet<LopHocPhan> LopHocPhans { get; set; }
        public virtual DbSet<SinhVien> SinhViens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BaoCaoCuaSinhVien>()
                .Property(e => e.MaSinhVien)
                .IsUnicode(false);

            modelBuilder.Entity<BaoCaoCuaSinhVien>()
                .Property(e => e.MaDoanhNghiep)
                .IsUnicode(false);

            modelBuilder.Entity<BaoCaoCuaSinhVien>()
                .Property(e => e.MaGiangVien)
                .IsUnicode(false);

            modelBuilder.Entity<ChuyenNganh>()
                .Property(e => e.MaChuyenNganh)
                .IsUnicode(false);

            modelBuilder.Entity<ChuyenNganh>()
                .Property(e => e.MaKhoa)
                .IsUnicode(false);

            modelBuilder.Entity<ChuyenNganh>()
                .HasMany(e => e.SinhViens)
                .WithRequired(e => e.ChuyenNganh)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DoanhNghiep>()
                .Property(e => e.MaDoanhNghiep)
                .IsUnicode(false);

            modelBuilder.Entity<DoanhNghiep>()
                .HasMany(e => e.SinhViens)
                .WithRequired(e => e.DoanhNghiep)
                .HasForeignKey(e => e.MaDoanhNghiepThucTap)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GiangVien>()
                .Property(e => e.MaGiangVien)
                .IsUnicode(false);

            modelBuilder.Entity<GiangVien>()
                .HasMany(e => e.LopHocPhans)
                .WithRequired(e => e.GiangVien)
                .HasForeignKey(e => e.MaGiangVienHuongDan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GiangVien>()
                .HasMany(e => e.SinhViens)
                .WithRequired(e => e.GiangVien)
                .HasForeignKey(e => e.MaGiangVienHuongDan)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Khoa>()
                .Property(e => e.MaKhoa)
                .IsUnicode(false);

            modelBuilder.Entity<Khoa>()
                .HasMany(e => e.ChuyenNganhs)
                .WithRequired(e => e.Khoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Khoa>()
                .HasMany(e => e.LopHocPhans)
                .WithRequired(e => e.Khoa)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LopHocPhan>()
                .Property(e => e.MaLopHocPhan)
                .IsUnicode(false);

            modelBuilder.Entity<LopHocPhan>()
                .Property(e => e.MaGiangVienHuongDan)
                .IsUnicode(false);

            modelBuilder.Entity<LopHocPhan>()
                .Property(e => e.MaKhoa)
                .IsUnicode(false);

            modelBuilder.Entity<SinhVien>()
                .Property(e => e.MaSinhVien)
                .IsUnicode(false);

            modelBuilder.Entity<SinhVien>()
                .Property(e => e.MaChuyenNganh)
                .IsUnicode(false);

            modelBuilder.Entity<SinhVien>()
                .Property(e => e.MaGiangVienHuongDan)
                .IsUnicode(false);

            modelBuilder.Entity<SinhVien>()
                .Property(e => e.MaDoanhNghiepThucTap)
                .IsUnicode(false);

            modelBuilder.Entity<SinhVien>()
                .Property(e => e.MaLopHocPhan)
                .IsUnicode(false);

            modelBuilder.Entity<SinhVien>()
                .HasMany(e => e.BaoCaoCuaSinhViens)
                .WithRequired(e => e.SinhVien)
                .WillCascadeOnDelete(false);
        }
    }
}
