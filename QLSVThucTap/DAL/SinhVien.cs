namespace QLSVThucTap.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SinhVien")]
    public partial class SinhVien
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SinhVien()
        {
            BaoCaoCuaSinhViens = new HashSet<BaoCaoCuaSinhVien>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int STT { get; set; }

        [Key]
        [StringLength(50)]
        public string MaSinhVien { get; set; }

        [Required]
        [StringLength(50)]
        public string Ho { get; set; }

        [Required]
        [StringLength(50)]
        public string Ten { get; set; }

        [Required]
        [StringLength(50)]
        public string MaChuyenNganh { get; set; }

        [Required]
        [StringLength(50)]
        public string MaGiangVienHuongDan { get; set; }

        [Required]
        [StringLength(50)]
        public string MaDoanhNghiepThucTap { get; set; }

        [Required]
        [StringLength(50)]
        public string MaLopHocPhan { get; set; }

        public DateTime? NamSinh { get; set; }

        public bool? GioiTinh { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BaoCaoCuaSinhVien> BaoCaoCuaSinhViens { get; set; }

        public virtual ChuyenNganh ChuyenNganh { get; set; }

        public virtual DoanhNghiep DoanhNghiep { get; set; }

        public virtual GiangVien GiangVien { get; set; }
    }
}
