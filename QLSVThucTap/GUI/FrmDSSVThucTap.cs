﻿using ClosedXML.Excel;
using QLSVThucTap.BLL;
using QLSVThucTap.GUI;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using static QLSVThucTap.BLL.SinhVienBLL;
using Excel = Microsoft.Office.Interop.Excel;


namespace QLSVThucTap
{
    public partial class FrmDSSVThucTap : Form
    {
        public FrmDSSVThucTap()
        {
            InitializeComponent();
        }

        public void NapTatCaSinhVien()
        {
            var ListSV = SinhVienBLL.getAllSinhVien();
            sinhVienVMBindingSource.DataSource = ListSV;
            dataGridViewMain.DataSource = sinhVienVMBindingSource;
        }
        public void NapSinhVienByKhoa(string makhoa)
        {
            var ListSV = SinhVienBLL.GetSVByIdKhoa(makhoa);
            sinhVienVMBindingSource.DataSource = ListSV;
            dataGridViewMain.DataSource = sinhVienVMBindingSource;
        }

        public void NapSinhVienByLop(string malophocphan)
        {
            var ListSV = SinhVienBLL.GetSVByIdLop(malophocphan);
            sinhVienVMBindingSource.DataSource = ListSV;
            dataGridViewMain.DataSource = sinhVienVMBindingSource;
        }

        public void loadTreeView()
        {
            treeView1.Nodes.Clear();
            TreeNode nodeAllKhoa = new TreeNode("Danh sách khoa");
            treeView1.Nodes.Add(nodeAllKhoa);
            var listKhoa = KhoaBLL.GetListKhoaVM();
            foreach (KhoaVM item in listKhoa)
            {
                TreeNode nodekhoa = new TreeNode(item.TenKhoa);
                nodekhoa.Tag = item;
                nodeAllKhoa.Nodes.Add(nodekhoa);

                var listLop = LopHocPhanBLL.GetLopByIdKhoa(item.MaKhoa);
                foreach (LopHocPhanVM itemLop in listLop)
                {
                    TreeNode nodeLop = new TreeNode(itemLop.TenLopHocPhan);
                    nodeLop.Tag = itemLop;
                    nodekhoa.Nodes.Add(nodeLop);
                }
            }
            treeView1.ExpandAll();
        }


        private void HienThiLenComBoBox()
        {

            cmbChuyenNganh.Items.Clear();
            foreach (KhoaVM k in KhoaBLL.GetListKhoaVM())
            {
                cmbKhoa.Items.Add(k);
                cmbKhoa.DisplayMember = "TenKhoa";
                cmbKhoa.ValueMember = "MaKhoa";
            }

            cmbCongty.Items.Clear();
            foreach (DoanhNghiepVM d in DoanhNghiepBLL.GetListDoanhNghiepVM())
            {
                cmbCongty.Items.Add(d);
                cmbCongty.DisplayMember = "TenDoanhNghiep";
                cmbCongty.ValueMember = "MaDoanhNghiep";
            }

            cmbGiangVien.Items.Clear();
            foreach (GiangVienVM d in GiangVienBLL.GetListGiangVienVM())
            {
                cmbGiangVien.Items.Add(d);
                cmbGiangVien.DisplayMember = "TenGiangVien";
                cmbGiangVien.ValueMember = "MaGiangVien";
            }

            cmbMaNhom.Items.Clear();
            foreach (LopHocPhanVM d in LopHocPhanBLL.GetListLopHocPhanByIdKhoa())
            {
                cmbMaNhom.Items.Add(d);
                cmbMaNhom.DisplayMember = "TenLopHocPhan";
                cmbMaNhom.ValueMember = "MaLopHocPhan";
            }

            cmbChuyenNganh.Items.Clear();
            foreach (ChuyenNganhVM d in ChuyenNganhBLL.GetListChuyenNganhByIdKhoa())
            {
                cmbChuyenNganh.Items.Add(d);
                cmbChuyenNganh.DisplayMember = "TenChuyenNganh";
                cmbChuyenNganh.ValueMember = "MaChuyenNganh";
            }
        }

        private void FrmThucTapSV_Load(object sender, EventArgs e)
        {
            loadTreeView();
            HienThiLenComBoBox();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null)
            {
                if (e.Node.Level == 0)
                {
                    NapTatCaSinhVien();
                }
                else if (e.Node.Level == 1)
                {
                    KhoaVM khoa = e.Node.Tag as KhoaVM;
                    NapSinhVienByKhoa(khoa.MaKhoa);
                }
                else if (e.Node.Level == 2)
                {
                    LopHocPhanVM lh = e.Node.Tag as LopHocPhanVM;
                    NapSinhVienByLop(lh.MaLopHocPhan);
                }
                else
                {
                    dataGridViewMain.DataSource = null;
                }
            }
        }

        private void exportDanhSachSV(object sender, EventArgs e)
        {
            saveFileDialog1.DefaultExt = ".xlsx";
            saveFileDialog1.Filter = "Excel Files|*.xlsx;*.xlsm";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK && dataGridViewMain.Rows.Count > 0)
            {
                try
                {

                    Excel.Application xcel = new Excel.Application();
                    xcel.Application.Workbooks.Add(Type.Missing);
                    for (int i = 1; i < dataGridViewMain.Columns.Count + 1; i++)
                    {
                        xcel.Cells[1, i] = dataGridViewMain.Columns[i - 1].HeaderText;
                    }

                    for (int i = 0; i < dataGridViewMain.Rows.Count; i++)
                    {
                        for (int j = 0; j < dataGridViewMain.Columns.Count; j++)
                        {
                            xcel.Cells[i + 2, j + 1] = dataGridViewMain.Rows[i].Cells[j].Value;
                        }
                    }
                    Excel.Worksheet worksheet = xcel.Worksheets["Sheet1"];
                    worksheet.Name = "Danh Sách Sinh Viên";
                    xcel.Columns.AutoFit();
                    xcel.Visible = true;

                    MessageBox.Show("Export data thành công", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void HienThiLenComboBoxChuyenNganh(KhoaVM khoa)
        {
            cmbChuyenNganh.Items.Clear();
            foreach (ChuyenNganhVM c in ChuyenNganhBLL.GetListChuyenNganhByIdKhoa(khoa.MaKhoa))
            {
                cmbChuyenNganh.Items.Add(c);
                cmbChuyenNganh.DisplayMember = "TenChuyenNganh";
                cmbChuyenNganh.ValueMember = "MaChuyenNganh";
            }
        }

        private void HienThiLenComboBoxLopHocPhan(KhoaVM khoa)
        {
            cmbMaNhom.Items.Clear();
            foreach (LopHocPhanVM l in LopHocPhanBLL.GetListLopHocPhanByIdKhoa(khoa.MaKhoa))
            {
                cmbMaNhom.Items.Add(l);
                cmbMaNhom.DisplayMember = "TenLopHocPhan";
                cmbMaNhom.ValueMember = "MaLopHocPhan";
            }
        }

        private void cmbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbKhoa.SelectedIndex == -1)
            {
                return;
            }
            var item = cmbKhoa.SelectedItem as KhoaVM;
            HienThiLenComboBoxChuyenNganh(item);
            HienThiLenComboBoxLopHocPhan(item);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            var MaSinhVien = txtMSV.Text;
            var Ho = txtHo.Text;
            var Ten = txtTen.Text;
            DateTime NgayS = dateTimePickerNS.Value;
            var ChuyenNganh = cmbChuyenNganh.SelectedItem as ChuyenNganhVM;
            var Khoa = cmbKhoa.SelectedItem as KhoaVM;
            var Lop = cmbMaNhom.SelectedItem as LopHocPhanVM;
            var GiangVien = cmbGiangVien.SelectedItem as GiangVienVM;
            var DoanhNghiep = cmbCongty.SelectedItem as DoanhNghiepVM;
            var GioiTinh = rdbNam.Checked ? true : false;


            if (string.IsNullOrEmpty(MaSinhVien))
            {
                errorProvider1.SetError(txtMSV, "Mã sinh viên không được để trống");
                return;
            }

            if (string.IsNullOrEmpty(Ho) || string.IsNullOrEmpty(Ten))
            {
                errorProvider1.SetError(txtHo, "Họ tên sinh viên không được để trống");
                return;
            }

            if (cmbChuyenNganh.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn chưa chọn chuyên ngành");
                return;
            }

            if (cmbKhoa.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn chưa chọn khoa");
                return;
            }

            if (cmbGiangVien.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn chưa chọn giảng viên");
                return;
            }

            if (cmbMaNhom.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn chưa chọn lớp học phần");
                return;
            }

            if (cmbCongty.SelectedIndex == -1)
            {
                MessageBox.Show("Bạn chưa chọn doanh nghiệp");
                return;
            }

            //Thêm mới dữ liệu
            var rs = SinhVienBLL.AddSV(new SinhVienVM
            {
                MaSinhVien = MaSinhVien,
                Ho = Ho,
                Ten = Ten,
                NamSinh = NgayS.Date,
                GioiTinh = GioiTinh ? "Nữ" : "Nam",
                MaChuyenNganh = ChuyenNganh.MaChuyenNganh,
                MaGiangVienHuongDan = GiangVien.MaGiangVien,
                MaDoanhNghiep = DoanhNghiep.MaDoanhNghiep
            }, Lop.MaLopHocPhan);

            if (rs == KETQUA.THANHCONG)
            {
                MessageBox.Show("Thêm sinh viên thành công !!!", "Thông báo");
                NapSinhVienByLop(Lop.MaLopHocPhan);
            }
            else if (rs == KETQUA.TRUNGMASV)
            {
                MessageBox.Show("Mã sinh viên này đã tồn tại !!!", "Thông báo");
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            var SelectedSinhVien = sinhVienVMBindingSource.Current as SinhVienVM;
            if (SelectedSinhVien != null)
            {
                var rs = MessageBox.Show("Bạn có thật sự muốn xoá", "Chú ý", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (rs == DialogResult.OK)
                {
                    SinhVienBLL.DeleteSinhVien(SelectedSinhVien.MaSinhVien);
                    sinhVienVMBindingSource.RemoveCurrent();
                    MessageBox.Show("Đã xoá sinh viên thành công", "Thông báo");
                }
            }
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            var SelectedSinhVien = sinhVienVMBindingSource.Current as SinhVienVM;
            if (SelectedSinhVien != null)
            {
                txtHo.Text = SelectedSinhVien.Ho;
                txtTen.Text = SelectedSinhVien.Ten;
                txtMSV.Text = SelectedSinhVien.MaSinhVien;
                dateTimePickerNS.Value = SelectedSinhVien.NamSinh;
                cmbKhoa.Text = SelectedSinhVien.TenKhoa;
                cmbGiangVien.Text = SelectedSinhVien.TenGiangVienHuongDan;
                cmbChuyenNganh.Text = SelectedSinhVien.TenChuyenNganh;
                cmbMaNhom.Text = SelectedSinhVien.TenLopHocPhan;
                cmbCongty.Text = SelectedSinhVien.TenDoanhNghiep;
                if (SelectedSinhVien.GioiTinh == "Nam")
                    rdbNam.Checked = true;
                else
                    rdbNu.Checked = true;

            }
            else
            {
                txtHo.Clear();
                txtTen.Clear();
                txtMSV.Clear();
                cmbKhoa.ResetText();
                cmbGiangVien.ResetText();
                cmbCongty.ResetText();
                cmbMaNhom.ResetText();
                cmbChuyenNganh.ResetText();
            }
        }

        private void baoCaoSVToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new FrmBaoCaoSV();
            frm.Show();
        }

        private void btnImportDSSV_Click(object sender, EventArgs e)
        {

            dataGridViewMain.Hide();

            string file = "";   //variable for the Excel File Location
            DataTable dt = new DataTable();   //container for our excel data
            DataRow row;

            openFileDialog1.DefaultExt = ".xlsx";
            openFileDialog1.Filter = "Excel Files|*.xlsx;*.xlsm";


            if (openFileDialog1.ShowDialog() == DialogResult.OK)   // Check if Result == "OK".
            {
                file = openFileDialog1.FileName; //get the filename with the location of the file
                try

                {
                    //Create Object for Microsoft.Office.Interop.Excel that will be use to read excel file

                    Excel.Application excelApp = new Excel.Application();

                    Excel.Workbook excelWorkbook = excelApp.Workbooks.Open(file);

                    Excel._Worksheet excelWorksheet = excelWorkbook.Sheets[1];

                    Excel.Range excelRange = excelWorksheet.UsedRange;


                    int rowCount = excelRange.Rows.Count;  //get row count of excel data

                    int colCount = excelRange.Columns.Count; // get column count of excel data

                    //Get the first Column of excel file which is the Column Name                  

                    for (int i = 1; i <= rowCount; i++)
                    {
                        for (int j = 1; j <= colCount; j++)
                        {
                            dt.Columns.Add(excelRange.Cells[i, j].Value2.ToString());
                        }
                        break;
                    }


                    //Get Row Data of Excel              
                    int rowCounter;  //This variable is used for row index number
                    for (int i = 2; i <= rowCount; i++) //Loop for available row of excel data
                    {
                        row = dt.NewRow();  //assign new row to DataTable
                        rowCounter = 0;
                        for (int j = 1; j <= colCount; j++) //Loop for available column of excel data
                        {
                            //check if cell is empty

                            if (excelRange.Cells[i, j] != null && excelRange.Cells[i, j].Value2 != null)
                            {
                                row[rowCounter] = excelRange.Cells[i, j].Value2.ToString() + " Data";
                            }
                            else
                            {
                                row[i] = "";
                            }

                            rowCounter++;
                        }
                        dt.Rows.Add(row); //add row to DataTable
                    }

                    dataGridViewTest.DataSource = dt; //assign DataTable as Datasource for DataGridview

                    //Close and Clean excel process
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    Marshal.ReleaseComObject(excelRange);
                    Marshal.ReleaseComObject(excelWorksheet);
                    excelWorkbook.Close();
                    Marshal.ReleaseComObject(excelWorkbook);

                    //quit 
                    excelApp.Quit();
                    Marshal.ReleaseComObject(excelApp);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }
    }
}
