﻿using QLSVThucTap.BLL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLSVThucTap.GUI
{
    public partial class FrmBaoCaoSV : Form
    {
        public FrmBaoCaoSV()
        {
            InitializeComponent();
        }

        private void FrmBaoCaoSV_Load(object sender, EventArgs e)
        {
            loadTreeView();
        }

        public void loadTreeView()
        {
            treeView1.Nodes.Clear();
            TreeNode nodeAllKhoa = new TreeNode("Danh sách khoa");
            treeView1.Nodes.Add(nodeAllKhoa);
            var listKhoa = KhoaBLL.GetListKhoaVM();
            foreach (KhoaVM item in listKhoa)
            {
                TreeNode nodekhoa = new TreeNode(item.TenKhoa);
                nodekhoa.Tag = item;
                nodeAllKhoa.Nodes.Add(nodekhoa);

                var listLop = LopHocPhanBLL.GetLopByIdKhoa(item.MaKhoa);
                foreach (LopHocPhanVM itemLop in listLop)
                {
                    TreeNode nodeLop = new TreeNode(itemLop.TenLopHocPhan);
                    nodeLop.Tag = itemLop;
                    nodekhoa.Nodes.Add(nodeLop);
                }
            }
            treeView1.ExpandAll();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null)
            {
                if (e.Node.Level == 0)
                {
                    NapTatCaBaoCao();
                }
                else if (e.Node.Level == 1)
                {
                    KhoaVM khoa = e.Node.Tag as KhoaVM;
                    NapBaoCaoByKhoa(khoa.MaKhoa);
                }
                else if (e.Node.Level == 2)
                {
                    LopHocPhanVM lh = e.Node.Tag as LopHocPhanVM;
                    NapBaoCaoByLop(lh.MaLopHocPhan);
                }
                else
                {
                    dataGridViewBaoCao.DataSource = null;
                }
            }
        }

        private void NapBaoCaoByLop(string maLopHocPhan)
        {
            throw new NotImplementedException();
        }

        private void NapBaoCaoByKhoa(string maKhoa)
        {
            throw new NotImplementedException();
        }

        private void NapTatCaBaoCao()
        {
            var ListSV = SinhVienBLL.getAllBaoCao();
            baoCaoSinhVienVMBindingSource.DataSource = ListSV;
            dataGridViewBaoCao.DataSource = baoCaoSinhVienVMBindingSource;
        }

        private void danhSachToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            var frm = new FrmDSSVThucTap();
            frm.Show();
        }
    }
}
