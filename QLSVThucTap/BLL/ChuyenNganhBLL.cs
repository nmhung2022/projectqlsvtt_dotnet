﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    internal class ChuyenNganhBLL
    {
        public static List<ChuyenNganhVM> GetListChuyenNganhByIdKhoa(string maKhoa = null)
        {
            QLSVTTModel model = new QLSVTTModel();
            if (maKhoa != null)
            {
                var ls = model.ChuyenNganhs.Where(c => c.MaKhoa == maKhoa).Select(e => new ChuyenNganhVM
                {
                    MaChuyenNganh = e.MaChuyenNganh,
                    TenChuyenNganh = e.TenChuyenNganh
                }).ToList();
                return ls;
            }
            var lsv = model.ChuyenNganhs.Select(e => new ChuyenNganhVM
            {
                MaChuyenNganh = e.MaChuyenNganh,
                TenChuyenNganh = e.TenChuyenNganh
            }).ToList();
            return lsv;
        }
    }
}
