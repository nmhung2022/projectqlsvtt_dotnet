﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    internal class SinhVienBLL
    {
        public enum KETQUA
        {
            THANHCONG, TRUNGMASV
        }

        public static List<SinhVienVM> GetSVByIdLop(string malophocphan)
        {
            QLSVTTModel model = new QLSVTTModel();

            var ls = from lp in model.LopHocPhans
                     join sv in model.SinhViens on lp.MaLopHocPhan equals sv.MaLopHocPhan
                     join gv in model.GiangViens on sv.MaGiangVienHuongDan equals gv.MaGiangVien
                     join ct in model.DoanhNghieps on sv.MaDoanhNghiepThucTap equals ct.MaDoanhNghiep
                     join cn in model.ChuyenNganhs on sv.MaChuyenNganh equals cn.MaChuyenNganh
                     where sv.MaLopHocPhan == malophocphan
                     select new
                     {
                         sv.STT,
                         sv.MaSinhVien,
                         sv.Ho,
                         sv.Ten,
                         sv.NamSinh,
                         gv.TenGiangVien,
                         cn.TenChuyenNganh,
                         ct.TenDoanhNghiep,
                         sv.GioiTinh
                     };
            return ls.Select(sv => new SinhVienVM
            {
                STT = sv.STT,
                MaSinhVien = sv.MaSinhVien,
                Ho = sv.Ho,
                Ten = sv.Ten,
                GioiTinh = (bool)sv.GioiTinh ? "Nam" : "Nữ",
                NamSinh = (DateTime)sv.NamSinh,
                TenGiangVienHuongDan = sv.TenGiangVien,
                TenChuyenNganh = sv.TenChuyenNganh,
                TenDoanhNghiep = sv.TenDoanhNghiep,

            }).ToList();
        }

        public static object getAllSinhVien()
        {
            QLSVTTModel model = new QLSVTTModel();

            var ls = from lp in model.LopHocPhans
                     join sv in model.SinhViens on lp.MaLopHocPhan equals sv.MaLopHocPhan
                     join gv in model.GiangViens on sv.MaGiangVienHuongDan equals gv.MaGiangVien
                     join ct in model.DoanhNghieps on sv.MaDoanhNghiepThucTap equals ct.MaDoanhNghiep
                     join cn in model.ChuyenNganhs on sv.MaChuyenNganh equals cn.MaChuyenNganh
                     select new
                     {
                         sv.STT,
                         sv.MaSinhVien,
                         sv.Ho,
                         sv.Ten,
                         sv.NamSinh,
                         gv.TenGiangVien,
                         cn.TenChuyenNganh,
                         ct.TenDoanhNghiep,
                         sv.GioiTinh
                     };
            return ls.Select(sv => new SinhVienVM
            {
                STT = sv.STT,
                MaSinhVien = sv.MaSinhVien,
                Ho = sv.Ho,
                Ten = sv.Ten,
                GioiTinh = (bool)sv.GioiTinh ? "Nam" : "Nữ",
                NamSinh = (DateTime)sv.NamSinh,
                TenGiangVienHuongDan = sv.TenGiangVien,
                TenChuyenNganh = sv.TenChuyenNganh,
                TenDoanhNghiep = sv.TenDoanhNghiep,

            }).ToList();
        }

        internal static List<BaoCaoSinhVienVM> getAllBaoCao()
        {
            QLSVTTModel model = new QLSVTTModel();

            var qr = from bc in model.BaoCaoCuaSinhViens
                     join sv in model.SinhViens on bc.MaSinhVien equals sv.MaSinhVien
                     join gv in model.GiangViens on sv.MaGiangVienHuongDan equals gv.MaGiangVien
                     join ct in model.DoanhNghieps on sv.MaDoanhNghiepThucTap equals ct.MaDoanhNghiep
                     join cn in model.ChuyenNganhs on sv.MaChuyenNganh equals cn.MaChuyenNganh
                     select new
                     {
                         bc.STT,
                         sv.MaSinhVien,
                         sv.Ho,
                         sv.Ten,
                         ct.TenDoanhNghiep,
                         gv.TenGiangVien,
                         bc.DeTai,
                         bc.NgayBatDau,
                         bc.NgayKetThuc,
                         bc.NoiDung,
                     };
            return qr.Select(bc => new BaoCaoSinhVienVM
            {
                STT = bc.STT,
                MaSinhVien = bc.MaSinhVien,
                Ho = bc.Ho,
                Ten = bc.Ten,
                TenDoanhNghiep = bc.TenDoanhNghiep,
                TenGiangVien = bc.TenGiangVien,
                DeTai = bc.DeTai,
                NgayBatDau = bc.NgayBatDau,
                NgayKetThuc = bc.NgayKetThuc,
                NoiDung = bc.NoiDung,
            }).ToList();
        }

        public static List<LopHocPhanVM> GetLopByIdKhoa(string makhoa)
        {
            QLSVTTModel model = new QLSVTTModel();

            var lsSV = model.LopHocPhans.Where(sv => sv.MaKhoa == makhoa).Select(sv => new LopHocPhanVM
            {
                MaKhoa = sv.MaKhoa,
                MaLopHocPhan = sv.MaLopHocPhan,
                TenLopHocPhan = sv.TenLopHocPhan,
            }).ToList();
            return lsSV;
        }

        public static List<SinhVienVM> GetSVByIdKhoa(string makhoa)
        {
            QLSVTTModel model = new QLSVTTModel();

            var qr = from lp in model.LopHocPhans
                     join sv in model.SinhViens on lp.MaLopHocPhan equals sv.MaLopHocPhan
                     join gv in model.GiangViens on sv.MaGiangVienHuongDan equals gv.MaGiangVien
                     join ct in model.DoanhNghieps on sv.MaDoanhNghiepThucTap equals ct.MaDoanhNghiep
                     join cn in model.ChuyenNganhs on sv.MaChuyenNganh equals cn.MaChuyenNganh
                     where lp.MaKhoa == makhoa
                     select new
                     {
                         sv.STT,
                         sv.MaSinhVien,
                         sv.Ho,
                         sv.Ten,
                         sv.NamSinh,
                         ct.TenDoanhNghiep,
                         gv.TenGiangVien,
                         sv.GioiTinh,
                         cn.TenChuyenNganh
                     };
            return qr.Select(sv => new SinhVienVM
            {
                STT = sv.STT,
                MaSinhVien = sv.MaSinhVien,
                Ho = sv.Ho,
                Ten = sv.Ten,
                NamSinh = (DateTime)sv.NamSinh,
                TenDoanhNghiep = sv.TenDoanhNghiep,
                TenGiangVienHuongDan = sv.TenGiangVien,
                GioiTinh = (bool)sv.GioiTinh ? "Nam" : "Nữ",
                TenChuyenNganh = sv.TenChuyenNganh,

            }).ToList();
        }

        public static KETQUA AddSV(SinhVienVM sv, string maLop)
        {
            try
            {
                QLSVTTModel model = new QLSVTTModel();
                var svM = model.SinhViens.Where(e => e.MaSinhVien == sv.MaSinhVien).FirstOrDefault();
                if (svM != null)
                {
                    return KETQUA.TRUNGMASV;
                }
                svM = new SinhVien
                {
                    MaSinhVien = sv.MaSinhVien,
                    Ho = sv.Ho,
                    Ten = sv.Ten,
                    NamSinh = sv.NamSinh,
                    GioiTinh = sv.GioiTinh == "Nam" ? false : true,
                    MaChuyenNganh = sv.MaChuyenNganh,
                    MaDoanhNghiepThucTap = sv.MaDoanhNghiep,
                    MaGiangVienHuongDan = sv.MaGiangVienHuongDan,
                    MaLopHocPhan = maLop,
                };
                model.SinhViens.Add(svM);
                model.SaveChanges();
                return KETQUA.THANHCONG;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public static void DeleteSinhVien(string maSinhVien)
        {
            try
            {
                QLSVTTModel model = new QLSVTTModel();
                var sv = model.SinhViens.Where(e => e.MaSinhVien == maSinhVien).FirstOrDefault();
                if (sv != null)
                {
                    model.SinhViens.Remove(sv);
                }
                else
                    throw new Exception("Sinh viên không tồn tại");
                model.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
        }
    }
}
