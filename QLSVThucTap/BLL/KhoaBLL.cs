﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    internal class KhoaBLL
    {
        public static List<KhoaVM> GetListKhoaVM()
        {
            QLSVTTModel model = new QLSVTTModel();

            var ls = from k in model.Khoas
                     select new
                     {
                         k.MaKhoa,
                         k.TenKhoa,
                     };
            return ls.Select(k => new KhoaVM
            {
                MaKhoa = k.MaKhoa,
                TenKhoa = k.TenKhoa
                
            }).ToList();
        }
    }
}
