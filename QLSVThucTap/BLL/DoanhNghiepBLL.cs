﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    internal class DoanhNghiepBLL
    {
        public static List<DoanhNghiepVM> GetListDoanhNghiepVM()
        {
            QLSVTTModel model = new QLSVTTModel();

            var ls = model.DoanhNghieps.Select(e => new DoanhNghiepVM
            {
                MaDoanhNghiep = e.MaDoanhNghiep,
                TenDoanhNghiep = e.TenDoanhNghiep
            }).ToList();
            return ls;
        }
    }
}
