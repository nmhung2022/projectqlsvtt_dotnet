﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    internal class LopHocPhanBLL
    {
        public static List<LopHocPhanVM> GetLopByIdKhoa(string maKhoa)
        {
            QLSVTTModel model = new QLSVTTModel();

            var lsSV = model.LopHocPhans.Where(lop => lop.MaKhoa == maKhoa).Select(l => new LopHocPhanVM
            {
                MaLopHocPhan = l.MaLopHocPhan,
                TenLopHocPhan = l.TenLopHocPhan,
            }).ToList();
            return lsSV;
        }

        public static List<LopHocPhanVM> GetListLopHocPhanByIdKhoa(string maKhoa = null)
        {
            QLSVTTModel model = new QLSVTTModel();
            if (maKhoa != null)
            {
                var ls = model.LopHocPhans.Where(c => c.MaKhoa == maKhoa).Select(e => new LopHocPhanVM
                {
                    MaLopHocPhan = e.MaLopHocPhan,
                    TenLopHocPhan = e.TenLopHocPhan
                }).ToList();
                return ls;
            }

            var lsv = model.LopHocPhans.Select(e => new LopHocPhanVM
            {
                MaLopHocPhan = e.MaLopHocPhan,
                TenLopHocPhan = e.TenLopHocPhan
            }).ToList();
            return lsv;

        }
    }
}
