﻿using QLSVThucTap.DAL;
using QLSVThucTap.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLSVThucTap.BLL
{
    internal class GiangVienBLL
    {
        public static List<GiangVienVM> GetListGiangVienVM()
        {
            QLSVTTModel model = new QLSVTTModel();

            var ls = model.GiangViens.Select(e => new GiangVienVM
            {
                MaGiangVien = e.MaGiangVien,
                TenGiangVien = e.TenGiangVien
            }).ToList();
            return ls;
        }
    }
}
